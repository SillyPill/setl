# Setl

Setl is a scripting language and a framework designed for managing shared
expenses among friends and settle debts. It is designed to be flexible and
accommodate all types of shared expenses.

## Features

- Simple way to calculate cashflow among groups
- Can handle complex use cases such as - expenses may or may not be shared among
  everyone
- Can alias sub groups
- It works on the cli 😎

# How to use

The idea is that the users record all their expenses in some type of
collaborative text file, using a simple, intuitive syntax. Then, setl calculates
all the settlements in the end.

## `setl` command

1. Create a plain text file detailing the expenses using Setl syntax. Let's say
  ` foo.setl `
2. ` $ setl foo.setl `

By default, if no FILENAME(s) is given, `setl` defaults to `STDIN`

## Syntax

### **To add a Dude**
```vcl
+ Foo
```

### To add an expense 

- **Shared among everyone**
```vcl
Foo: 100 @ Meal
```
Here, we are saying 'Foo' paid amount (units) 100 for a meal.

If multiple people payed
```vcl
Foo:100 Bar:100 @ Meal
```
Both 'Foo' and 'Bar' paid amount (units) 100 for a meal.

- **Shared among specific people**
```vcl
Foo:80 /Baz @ Ice Cream
```
Only 'Foo' and 'Baz' had Ice Cream and Foo paid (units) 80 for it

If 'Foo' didn't eat the Ice Cream, but paid for 'Bar' and 'Baz', use a `!` in
place of a `:`
```vcl
Foo!80 /Bar, Baz @ Ice Cream
```

### Aliasing
You can also alias a group if they tend to spend with each other a lot.

Example:
```vcl
BB(Bar, Baz)

Foo!80 /BB @ Ice Cream
Foo!40 /BB @ Chocolates
```

### Remove a Dude
In case he left early, for instance.
```vcl
- Foo
```

### Comments

Lines that start with `#` are ignored.

# Example

```vcl
+Anand
+Karpov
+Kasparov
+Carlsen
+Botvinnik

# We can alias groups together to represent groups
Russians(Karpov, Kasparov, Botvinnik)

Botvinnik:130 @ Train Fare
Anand: 100, Carlsen: 100 @ Food

# Fischer joined late
+Fischer

# Fischer joined the Russians and paid for the vodka
Fischer:150 /Russians @Vodka

Karpov: 225 @ Dinner

# Anand had to leave early, but didn't have cash on him, so Carlsen paid
# Botvinnik left with Anand
Carlsen!70 /Anand, Botvinnik @ Cab

-Anand
-Botvinnik

# Fischer and Karpov paid for the hat Carlsen wanted to buy
Fischer!30, Karpov!20 /Carlsen @ Hat Shop

```

See output [here.](./output.txt)

# Dependencies

- `Python3`
 
